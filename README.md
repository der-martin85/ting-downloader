# ting-downloader

Python 3.11 Skript zum runterladen von Ting Büchern und Linux Systemen. Es ist eine Weiterentwicklung des Skriptes vom Nutzer howil001 aus dem ubuntuusers.de Forum.

Die Bücher sollten vorher mit dem Stift gescant werden. Dabei werden die IDs der Bücher in die Datei TBD.TXT auf dem Stift geschrieben. Das Script prüft wo der Stift gemounted ist und schaut in die Datei welche Bücher geladen werden müssen. 
