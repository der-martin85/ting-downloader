#!/usr/bin/python3

from urllib.request import urlopen
from os.path import exists
from re import findall
import hashlib
from subprocess import run, PIPE
from sys import platform as _platform

# Check linux platform and find mountpoint
if _platform == "linux" or _platform == "linux2":
    # Execute mount command
    mount = run(['mount', '-v'], stdout=PIPE).stdout.decode('utf-8')
    lines = filter(lambda l: l != "", mount.split('\n'))
    points = map(lambda line: line.split(' ')[2], lines)

    # Check all found mount points
    n = -1
    for a in enumerate(points):
        if 'TING' in a[1].upper():
            n =a[0]
            MountedTingPath = a[1]+'/$ting'
            if exists(MountedTingPath):
                print('Found Ting mounted to "'+MountedTingPath+'"')
                break

    if n ==-1:
        print('Could not find mounted Ting. Please connect Ting to System.')
        quit()

else:
    print('Could not find Linux system!')
    quit()

config = {}
with open(MountedTingPath + "/settings.ini", 'r', encoding='utf-16') as configFile:
    lines = configFile.readlines()
    for line in lines:
        if "=" in line:
            parts = line.split("=")
            config[parts[0].strip()] = parts[1].strip()

#set global parameters
TingURL = config['server'] + "/book-files"
TingFileTypes = ["Thumb", "File", "Script"]
area = config['area']

TingFileDestDict = {
    "Desc":     MountedTingPath + "/{}_" + area + ".txt",
    "Thumb":    MountedTingPath + "/{}_" + area + ".png",
    "File":     MountedTingPath + "/{}_" + area + ".ouf",
    "Script":   MountedTingPath + "/{}_" + area + ".src"
}

TingFileSourceDict = {
    "Desc":     "http://" + TingURL + "/get-description/id/{}/area/" + area + "/",
    "Thumb":    "http://" + TingURL + "/get/id/{}/area/" + area + "/type/thumb",
    "File":     "http://" + TingURL + "/get/id/{}/area/" + area + "/type/archive",
    "Script":   "http://" + TingURL + "/get/id/{}/area/" + area + "/type/script"
}

def GetBookIDs(TBDFilePath=MountedTingPath+"/TBD.TXT"):
    with open(TBDFilePath, "r") as TBDFile:
        BookIDs = [i.strip() for i in TBDFile.readlines()]
    return BookIDs

def CheckForBookDesciptionFile(BookID):
    return exists(MountedTingPath+"/"+BookID+"_en.txt")

def GetBookMD5Sums(BookID):
    ResultDict = dict.fromkeys(TingFileTypes)
    Name = None

    with open(MountedTingPath+"/"+BookID+"_en.txt") as aFile:
        Lines = aFile.readlines()

    for Line in Lines:
        Match = findall("Name: (.*)", Line)
        if Match:
            Name = Match[0]
        for key in ResultDict:
            Match = findall(key+"MD5: ([0-9,a-f]+)", Line)
            if Match:
                ResultDict[key] = Match[0]
                break
    return ResultDict, Name

def CheckForBookFileValid(BookID, FileType, MD5SUM):
    LocalFilePath = TingFileDestDict[FileType].format(BookID)

    if exists(LocalFilePath):
        md5 = hashlib.md5()
        with open(LocalFilePath, "rb") as File:
            md5.update(File.read())
        isValid = (MD5SUM == md5.hexdigest())
        if not isValid:
            print(f" > {FileType} corrupt: expected {MD5SUM} != {md5.hexdigest()}, File: {LocalFilePath}")
        else:
            print(f" > {FileType} OK: {MD5SUM}")
        return isValid
    else:
        print(f" > {FileType} missing: {MD5SUM}")
        return False

def GetBookFile(BookID, FileType):
    LocalFilePath = TingFileDestDict[FileType].format(BookID)
    FullURL = TingFileSourceDict[FileType].format(BookID.lstrip("0"))
    print(f" > Start Download from {FullURL}")
    try:
        with urlopen(FullURL) as response:
            data = response.read()
        with open(LocalFilePath, 'wb') as out_file:
            out_file.write(data)
        return True
    except Exception as e:
        print(f" > getting {FullURL} to {LocalFilePath} failed")
        print(e)
        return False

BookIDs = GetBookIDs()
print(f"Found followings BookIDs for processing: {', '.join(BookIDs)}")

for BookID in BookIDs:
    if not(CheckForBookDesciptionFile(BookID)):
        if not GetBookFile(BookID, "Desc"):
            print(f"Description of Book with ID {BookID} could not be loaded.")
            continue
    MD5Sums, Name = GetBookMD5Sums(BookID)

    print(f'Checking content of Book {BookID} named "{Name}"')
    for retries in range(2):
        if retries > 0:
            print("retry!")
        allOk = True
        for key, value in MD5Sums.items():
            if value != None:
                if not(CheckForBookFileValid(BookID, key, value)):
                    GetBookFile(BookID, key)
                    if not(CheckForBookFileValid(BookID, key, value)):
                        print(f"Error: download failed for {key}")
                        allOk = False
        if allOk:
            print("Book done!")
            break;
    if not allOk:
        print(f'Book {Name} could not be loaded! Try another time.')
    print("-------------------------------")

print("All done!")
